package com.cyh.jsbridgex5.http;

public class LoginBeanData {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "LoginBeanData{" +
                "token='" + token + '\'' +
                '}';
    }
}
