package com.cyh.jsbridgex5;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.cyh.jsbridgex5.http.GlobalParams;


/**
 * app 闪屏页
 */

public class SplashActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sp = getSharedPreferences("isLogin", Context.MODE_PRIVATE);
        if (sp.getString("token", null) == null){
            startActivity(new Intent(this, LoginActivity.class));
        }else {
            GlobalParams.token = sp.getString("token", null);
            //startActivity(new Intent(this, HomeActivity.class));
            startActivity(new Intent(this, LoginActivity.class));
        }
    }



}

