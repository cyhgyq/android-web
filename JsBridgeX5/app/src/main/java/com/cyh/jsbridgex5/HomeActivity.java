package com.cyh.jsbridgex5;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;


/**
 * author: zhouyh
 * created on: 2020/9/11 4:47 PM
 * description: 首页
 */

public class HomeActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }
}
