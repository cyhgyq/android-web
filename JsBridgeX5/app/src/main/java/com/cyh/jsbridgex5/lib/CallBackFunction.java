package com.cyh.jsbridgex5.lib;

public interface CallBackFunction {
	
	public void onCallBack(String data);

}
