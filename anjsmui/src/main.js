import Vue from 'vue'
import 'amfe-flexible'; //引入rem自适应,替代了lib-flexible,并且适用大屏
//import 'lib-flexible'; //引用rem布局,因为这个布局只适配<540的布局，所以将里面的代码拷贝出来注释if(>540)的代码
//import '../static/flexible'
//import './utils/rem';
import 'normalize.css/normalize.css'
import MintUI from 'mint-ui'
import 'mint-ui/lib/style.css'
import App from './App.vue'
import store from './store'
import router from './router'

import '@/permission' // permission control

import Bridge from'./WebViewJavascriptBridge'
Vue.prototype.$bridge = Bridge

import './styles/mathjaxstyle.css'
import mathJaxVariable from './utils/MathJaxVariable'
Vue.prototype.mathJaxVar = mathJaxVariable

Vue.config.productionTip = false


Vue.use(MintUI)

import VueScroller from 'vue-scroller'
Vue.use(VueScroller)


//使用 fastclick解决移动端click事件300毫秒延迟方法
// 添加Fastclick移除移动端点击延迟
import FastClick from 'fastclick'
//FastClick的ios点击穿透解决方案
FastClick.prototype.focus = function (targetElement) {
    let length;
    if (targetElement.setSelectionRange && targetElement.type.indexOf('date') !== 0 && targetElement.type !== 'time' && targetElement.type !== 'month') {
        length = targetElement.value.length;
        targetElement.focus();
        targetElement.setSelectionRange(length, length);
    } else {
        targetElement.focus();
    }
};
 
FastClick.attach(document.body)


new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
})
