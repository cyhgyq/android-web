import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/api/teacher/info/login.json',
    method: 'post',
    data
  })
}

//https://segmentfault.com/a/1190000016788484 异步文章

/* export async function login(data) {
  return Promise.resolve(request({
    url: '/api/teacher/info/login.json',
    method: 'post',
    data
  })) 
} */

/* export async function login(data) {
  return request({
    url: '/api/teacher/info/login.json',
    method: 'post',
    data
  }) 
} */

/* const demo = async function () {
  return Promise.resolve('我是Promise');
  // 等同于 return '我是Promise'
  // 等同于 return new Promise((resolve,reject)=>{ resolve('我是Promise') })
} */

/* 因为async函数返回的是一个Promise，所以我们可以在外面catch住错误。

const demo = async ()=>{
  const result = await setDelay(1000);
  console.log(result);
  console.log(await setDelaySecond(2));
  console.log(await setDelay(1000));
  console.log('完成了');
}
demo().catch(err=>{
    console.log(err);
})
在async函数的catch中捕获错误，当做一个Pormise处理，同时你不想用这种方法，可以使用try...catch语句：

(async ()=>{
  try{
    const result = await setDelay(1000);
    console.log(result);
    console.log(await setDelaySecond(2));
    console.log(await setDelay(1000));
    console.log('完成了');
  } catch (e) {
    console.log(e); // 这里捕获错误
  }
})()
当然这时候你就不需要在外面catch了。 */

export function getInfo(token) {
  return request({
    url: '/vue-admin-template/user/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/vue-admin-template/user/logout',
    method: 'post'
  })
}
