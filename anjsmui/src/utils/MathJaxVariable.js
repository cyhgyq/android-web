let isMathjaxConfig = false;//用于标识是否配置,// 防止重复调用Config，造成性能损耗
const initMathjaxConfig = () => {
    if (!window.MathJax) {
        return;
    }
    window.MathJax.Hub.Config({
        showProcessingMessages: false, //关闭js加载过程信息
        messageStyle: "none", //不显示信息
        jax: ["input/TeX", "output/HTML-CSS", "output/PreviewHTML"],
        tex2jax: {
            inlineMath: [["$", "$"], ["\\(", "\\)"]], //行内公式选择符
            displayMath: [["$$", "$$"], ["\\[", "\\]"]], //段内公式选择符
            skipTags: ["script", "noscript", "style", "textarea", "pre", "code", "a"] //避开某些标签
        },
        "HTML-CSS": {
            availableFonts: ["STIX", "TeX"], //可选字体
            showMathMenu: false, //关闭右击菜单显示
            linebreaks: {
              automatic: true,  //超长公式换行处理（默认是false不换行）
              width: "90%"      //设置换行的点，默认是遇到等号=换行
            }  
        }
    });
    isMathjaxConfig = true; //配置完成，改为true
};

const MathQueue = function (elementId) {
  if (!window.MathJax) {
      return;
  }
  if (isMathjaxConfig === false) { // 如果：没有配置MathJax
    initMathjaxConfig();
  }
  let eId;
  if(!elementId) {
    eId = "app"; // 如果，不传入第三个参数，则渲染整个document, 因为使用的Vuejs，所以指明#app，以提高速度
  } else {
    eId = elementId
  }
  window.MathJax.Hub.Queue(["Typeset", window.MathJax.Hub, document.getElementById(eId)]);
};



export default {
    isMathjaxConfig,
    initMathjaxConfig,
    MathQueue,
}
