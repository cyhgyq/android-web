const getDefaultState = () => {
  return {
    name: '',
  }
}

const state = getDefaultState()

const mutations = {
  SET_NAME: (state, name) => {
    state.name = name
  },
}

const actions = {
  // user login
  setName({ commit, state }, name) {
    console.log(state.name);
    commit('SET_NAME', name)
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

